# Gymnasium

Some reinforcement learning with gumnasium


## Best results

### Q-learning
![Episode gif](/models_q_learning_1_1/array.gif)

### DQN
Approximates the Q function using a neural network
![Episode gif](/model_dqn_1_1/array.gif)
